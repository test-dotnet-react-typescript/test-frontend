export type TaskEntity = {
  id: string;
  title?: string;
  description?: string;
  dueDate: Date;
  isCompleted: boolean;
};
