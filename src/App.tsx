import "./App.css";

import React from "react";

import TaskTable from "./Components/TaskTable/TaskTable";

function App() {
  return (
    <div className="App">
      <h1>Developer Test Assignment </h1>
      <TaskTable />
    </div>
  );
}

export default App;
