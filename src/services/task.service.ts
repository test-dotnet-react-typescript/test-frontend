import { ApiClient } from "../apiClient/apClient";
import { TaskEntity } from "../Interfaces/task.interface";

const api = ApiClient.getInstance();

export const getTasks = async () => {
  try {
    const response = await api.get("/Task");
    return response.data;
  } catch (error) {
    console.log(error);
    return error;
  }
};

export const commmitTasks = async (tasks: TaskEntity[]) => {
  try {
    const response = await api.post("/Task", tasks);
    return response.data;
  } catch (error) {
    console.log(error);
    return error;
  }
};
